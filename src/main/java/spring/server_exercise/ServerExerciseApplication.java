package spring.server_exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerExerciseApplication.class, args);
	}

}
